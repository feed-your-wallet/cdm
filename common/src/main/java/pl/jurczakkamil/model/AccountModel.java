package pl.jurczakkamil.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.jurczakkamil.consts.AccountType;

import java.math.BigDecimal;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.NUMBER_FLOAT;
import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AccountModel extends BaseStoredInDbDataModel {

    @JsonFormat(shape = STRING)
    private AccountType type;

    @JsonFormat(shape = STRING)
    private String name;

    @JsonFormat(shape = NUMBER_FLOAT)
    private BigDecimal initialBalance;

    @JsonFormat(shape = STRING)
    private String icon;

    @JsonFormat(shape = STRING)
    private String color;
}
