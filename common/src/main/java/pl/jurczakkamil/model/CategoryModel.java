package pl.jurczakkamil.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CategoryModel extends BaseStoredInDbDataModel{

    @JsonFormat(shape = STRING)
    private String name;

    @JsonFormat(shape = STRING)
    private String icon;

    @JsonFormat(shape = STRING)
    private String color;
}
