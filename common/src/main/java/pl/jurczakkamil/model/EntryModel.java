package pl.jurczakkamil.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.jurczakkamil.consts.CurrencyType;
import pl.jurczakkamil.consts.EntryType;
import pl.jurczakkamil.consts.PaymentType;

import java.math.BigDecimal;
import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.NUMBER_FLOAT;
import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EntryModel extends BaseStoredInDbDataModel {

    @JsonFormat(shape = STRING)
    private EntryType type;
    private Long accountId;
    @JsonFormat(shape = NUMBER_FLOAT)
    private BigDecimal amount;
    @JsonFormat(shape = STRING)
    private CurrencyType currency;
    private Long categoryId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm")
    private Date date;
    private String description;
    @JsonFormat(shape = STRING)
    private PaymentType paymentType;
}
