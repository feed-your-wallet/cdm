package pl.jurczakkamil.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.NUMBER_INT;

@Data
@ToString
@EqualsAndHashCode
public abstract class BaseStoredInDbDataModel {

    @JsonFormat(shape = NUMBER_INT)
    private Long id;

    @JsonFormat(shape = NUMBER_INT)
    private Long version;
}
