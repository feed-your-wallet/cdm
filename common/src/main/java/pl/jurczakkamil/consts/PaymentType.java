package pl.jurczakkamil.consts;

public enum PaymentType {

    CASH,
    CARD,
    BANK_TRANSFER
}
