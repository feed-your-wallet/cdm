package pl.jurczakkamil.consts;

public enum EntryType {

    EXPENSE,
    INCOME,
    TRANSFER
}
