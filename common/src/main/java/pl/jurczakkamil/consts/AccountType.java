package pl.jurczakkamil.consts;

public enum AccountType {

    MAIN,
    CASH,
    BANK_ACCOUNT,
    SAVINGS_ACCOUNT
}
