package pl.jurczakkamil.service.cashflow.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import pl.jurczakkamil.BaseModel;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class AddEntryRequestModel extends BaseModel implements Serializable {

    private String type;
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Long accountId;
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_FLOAT)
    private String amount;
    private String currency;
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    private Long categoryId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm")
    private Date date;
    private String description;
    private String paymentType;
}
