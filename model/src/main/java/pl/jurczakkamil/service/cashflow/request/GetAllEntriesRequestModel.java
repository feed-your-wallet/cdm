package pl.jurczakkamil.service.cashflow.request;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.jurczakkamil.model.EntryModel;

import java.util.List;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class GetAllEntriesRequestModel {

    @JsonValue
    private List<EntryModel> entries;
}
