package pl.jurczakkamil.service.dictionary.response;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.jurczakkamil.model.AccountModel;

import java.util.List;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class AccountsResponseModel {

    @JsonValue
    private List<AccountModel> accounts;
}
