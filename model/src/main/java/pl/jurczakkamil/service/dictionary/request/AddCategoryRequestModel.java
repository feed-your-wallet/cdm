package pl.jurczakkamil.service.dictionary.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AddCategoryRequestModel {

    @JsonFormat(shape = STRING)
    private String name;

    @JsonFormat(shape = STRING)
    private String icon;

    @JsonFormat(shape = STRING)
    private String color;
}
