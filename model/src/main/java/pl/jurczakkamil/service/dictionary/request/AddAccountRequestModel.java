package pl.jurczakkamil.service.dictionary.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.jurczakkamil.consts.AccountType;

import java.math.BigDecimal;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.OBJECT;
import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AddAccountRequestModel {

    @JsonFormat(shape = STRING)
    private AccountType type;

    @JsonFormat(shape = STRING)
    private String name;

    @JsonFormat(shape = OBJECT)
    private BigDecimal initialBalance;

    @JsonFormat(shape = STRING)
    private String icon;

    @JsonFormat(shape = STRING)
    private String color;
}
