package pl.jurczakkamil.service.dictionary.response;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.jurczakkamil.model.CategoryModel;

import java.util.List;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class CategoriesResponseModel {

    @JsonValue
    private List<CategoryModel> categories;
}
